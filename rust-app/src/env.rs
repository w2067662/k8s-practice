use dotenv::dotenv;
use std::env;

//------------------------------
//             Env
//------------------------------
pub fn load_env() {
    // Load environment variables from a .env file
    dotenv().ok();
}

pub fn get_host() -> String {
    load_env();
    let host = env::var("HOST").unwrap_or("localhost".to_string());
    host
}

pub fn get_port() -> String {
    load_env();
    let port = env::var("PORT").unwrap_or("3280".to_string());
    port
}

pub fn get_mongo_host() -> String {
    load_env();
    let mongo_host = env::var("MONGO_HOST").unwrap_or("localhost".to_string());
    mongo_host
}

pub fn get_mongo_port() -> String {
    load_env();
    let mongo_port = env::var("MONGO_PORT").unwrap_or("27017".to_string());
    mongo_port
}

//------------------------------
//             Test
//------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_host() {
        load_env();
        let result = get_host();
        println!("Expected: 0.0.0.0, Actual: {}", result);
        assert_eq!(result, "0.0.0.0".to_string());
    }

    #[test]
    fn test_get_port() {
        load_env();
        let result = get_port();
        println!("Expected: 3280, Actual: {}", result);
        assert_eq!(result, "3280".to_string());
    }

    #[test]
    fn test_get_mongo_host() {
        load_env();
        let result = get_mongo_host();
        println!("Expected: localhost, Actual: {}", result);
        assert_eq!(result, "localhost".to_string());
    }

    #[test]
    fn test_get_mongo_port() {
        load_env();
        let result = get_mongo_port();
        println!("Expected: 27017, Actual: {}", result);
        assert_eq!(result, "27017".to_string());
    }
}
