use super::database::{email_exists, get_user_by_email, store_user};
use super::model::User;
use actix_web::{get, post, web, HttpRequest, HttpResponse, Responder};
use bson::DateTime;
use serde::{Deserialize, Serialize};

//------------------------------
//      API request Structs
//------------------------------
#[derive(Deserialize, Serialize)]
struct LoginRequest {
    email: String,
    password: String,
}

#[derive(Deserialize, Serialize)]
struct SignupRequest {
    name: String,
    email: String,
    password: String,
}

//------------------------------
//             APIs
//------------------------------
#[post("/signup")]
async fn signup(data: web::Json<SignupRequest>) -> impl Responder {
    let user_data = data.into_inner();

    // Check if the email already exists
    match email_exists(&user_data.email).await {
        Ok(true) => {
            return HttpResponse::Conflict().body("Email already exists.");
        }
        Ok(false) => {
            // Continue with user creation
        }
        Err(err) => {
            println!("{:?}", err);
            return HttpResponse::InternalServerError()
                .body(format!("Failed to check email existence."));
        }
    }

    let user = User {
        name: user_data.name,
        email: user_data.email,
        password: user_data.password,
        created_at: DateTime::now(),
    };

    if let Err(err) = store_user(user).await {
        println!("{:?}", err);
        return HttpResponse::InternalServerError().body(format!("Failed to store user."));
    }

    HttpResponse::Created().finish()
}

#[post("/login")]
async fn login(data: web::Json<LoginRequest>) -> impl Responder {
    let credentials = data.into_inner();

    if let Some(user) = get_user_by_email(&credentials.email).await.unwrap() {
        if user.password == credentials.password {
            return HttpResponse::Ok().json("Login successful!");
        }
    }

    HttpResponse::Unauthorized().json("Invalid username or password.")
}

#[get("/ping")]
async fn ping(req: HttpRequest) -> impl Responder {
    // realip_remote_addr() Real IP (remote address) of client that initiated request.
    if let Some(val) = req.connection_info().realip_remote_addr() {
        println!("Remote IP {:?}", val);
    };

    // peer_addr() serialized IP address of the peer connection.
    if let Some(val) = req.connection_info().peer_addr() {
        println!("Client IP {:?}", val);
    };

    HttpResponse::Ok().json("Hi! This is ping response from rust service.")
}
