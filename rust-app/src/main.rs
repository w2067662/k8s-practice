use actix_cors::Cors;
use actix_web::{http::header, http::Method, middleware::Logger, App, HttpServer};
use std::error::Error;

//------------------------------
//           Modules
//------------------------------
mod api;
mod database;
mod env;
mod model;

//------------------------------
//            Main
//------------------------------
#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Initialize the global MONGO_DB
    *database::MONGO_DB.lock().unwrap() = Some(database::connect_to_mongodb().await?);

    println!("Handle Requests on {}:{}", env::get_host(), env::get_port());

    HttpServer::new(move || {
        App::new()
            .wrap(
                Cors::default()
                    .allow_any_origin() // Equivalence to "*"
                    .allowed_methods(vec![
                        Method::GET,
                        Method::POST,
                        Method::PUT,
                        Method::DELETE,
                        Method::OPTIONS,
                    ])
                    .allowed_headers(vec![
                        header::ORIGIN,
                        header::CONTENT_TYPE,
                        header::CONTENT_LENGTH,
                    ])
                    .expose_headers(vec![header::CONTENT_LENGTH])
                    .supports_credentials()
                    .max_age(3600),
            )
            .wrap(Logger::default())
            .service(api::signup)
            .service(api::login)
            .service(api::ping)
    })
    .bind(format!("{}:{}", env::get_host(), env::get_port()))?
    .workers(2)
    .run()
    .await?;

    Ok(())
}

//------------------------------
//            Test
//------------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use tokio::test;

    #[test]
    async fn test_store_and_retrieve_user() {
        let mongo_db = database::connect_to_mongodb().await.unwrap();
        *database::MONGO_DB.lock().unwrap() = Some(mongo_db);

        let user = model::User {
            name: "example_user".to_string(),
            email: "user@example.com".to_string(),
            password: "password123".to_string(),
            created_at: bson::DateTime::now(),
        };

        database::store_user(user.clone()).await.unwrap();
        println!("model::User stored.");

        let email = "user@example.com";
        match database::get_user_by_email(email).await.unwrap() {
            Some(retrieved_user) => {
                // Compare model::User struct fields
                assert_eq!(retrieved_user.name, user.name);
                assert_eq!(retrieved_user.email, user.email);
                assert_eq!(retrieved_user.password, user.password);
            }
            None => panic!("model::User not found for email: {}", email),
        }
    }
}
