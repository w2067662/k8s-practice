use super::env::{get_mongo_host, get_mongo_port};
use super::model::User;
use bson::doc;
use lazy_static::lazy_static;
use mongodb::{
    error::Error,
    options::{ClientOptions, FindOneOptions},
    Client, Collection,
};
use std::sync::Mutex;

//------------------------------
//        Global Static
//------------------------------
lazy_static! {
    pub static ref MONGO_DB: Mutex<Option<MongoDatabase>> = Mutex::new(None);
}

const DATABASE_NAME: &str = "database";
const USER_COLLECTION_NAME: &str = "users";

//------------------------------
//      Database Structure
//------------------------------
pub struct MongoDatabase {
    pub user_collection: Collection<User>,
}

//------------------------------
//          Database
//------------------------------
pub async fn connect_to_mongodb() -> Result<MongoDatabase, Error> {
    let mongo_host = get_mongo_host();
    let mongo_port = get_mongo_port();

    // Construct the MongoDB connection string
    let mongodb_uri = format!("mongodb://{}:{}/", mongo_host, mongo_port);

    // Parse the connection string into ClientOptions
    let client_options = ClientOptions::parse(mongodb_uri).await?;

    // Create a MongoDB client
    let client = Client::with_options(client_options)?;

    // Access a specific database
    let db = client.database(DATABASE_NAME);

    // Access a collection within the database
    let user_collection = db.collection::<User>(USER_COLLECTION_NAME);

    Ok(MongoDatabase { user_collection })
}

//------------------------------
//        User functions
//------------------------------
pub async fn store_user(user: User) -> Result<(), Error> {
    let mongo_db = MONGO_DB.lock().unwrap();
    let mongo_db_ref = mongo_db.as_ref().unwrap();

    // Define the filter based on a unique identifier, for example, email
    let filter = doc! { "email": &user.email };

    // Create the update document using the provided user data
    let update_doc = doc! {
        "$set": bson::to_bson(&user).unwrap(),
    };

    // Set upsert to true, which means insert the document if no match is found
    let options = mongodb::options::UpdateOptions::builder()
        .upsert(true)
        .build();

    // Perform the upsert operation
    mongo_db_ref
        .user_collection
        .update_one(filter, update_doc, Some(options))
        .await?;

    Ok(())
}

pub async fn get_user_by_email(email: &str) -> Result<Option<User>, Error> {
    let filter = doc! { "email": email };
    let find_options = FindOneOptions::default(); // Use FindOneOptions with default settings

    let mongo_db = MONGO_DB.lock().unwrap();
    let mongo_db_ref = mongo_db.as_ref().unwrap();

    match mongo_db_ref
        .user_collection
        .find_one(filter, find_options)
        .await?
    {
        Some(user) => Ok(Some(user)),
        None => Ok(None),
    }
}

pub async fn email_exists(email: &str) -> Result<bool, Error> {
    // Define the filter based on the provided email
    let filter = doc! { "email": email };

    let mongo_db = MONGO_DB.lock().unwrap();
    let mongo_db_ref = mongo_db.as_ref().unwrap();

    // Find the document in the collection based on the filter
    match mongo_db_ref.user_collection.find_one(filter, None).await {
        Ok(Some(_)) => Ok(true), // Document with the provided email exists
        Ok(None) => Ok(false),   // Document with the provided email doesn't exist
        Err(err) => Err(err),    // Propagate the error
    }
}
