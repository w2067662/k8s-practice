use bson::DateTime;
use serde::{Deserialize, Serialize};

//------------------------------
//            Model
//------------------------------
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub name: String,
    pub email: String,
    pub password: String,
    pub created_at: DateTime,
}
