# 🌟 Kubernetes Practice (with tutorials)

This project demonstrates how to use a React server as a frontend-service to send requests to and receive responses from backend servers (go-service & rust-service) using Kubernetes (K8s) on Minikube. The README provides step-by-step instructions on setting up the environment, building and deploying the applications, and tracing data on pods. This project is also a study on how to build a single frontend with multiple backend applications and a practice of structural design using Kubernetes (K8s).

Feel free to rewrite the programs or use them as samples to build your own Kubernetes structure.

**Notice:** Additional links of information to help fix errors or address complex situations is available in the [reference](reference.txt) file.

## 📝 How to Use
1. Clone this repository.
2. Start minikube on docker.
3. Deploy the K8s cluster.
```bash
kubectl apply -f deployment.yaml
```
4. Start frontend service
```bash
minikube service frontend-service        # open in browser

minikube service frontend-service --url  # print URL on cmd
```

## 📑 Table of Contents
1. [Introduction](#-kubernetes-practice-with-tutorials)
2. [How to Use](#-how-to-use)
3. [Prerequisites](#-prerequisites)
4. [Preview](#-preview-web-demonstration)
5. [Structure](#-structure)
6. [Tutorial](#-tutorial)
   - [Step 1: Build Backend Server](#️⤵️-step1-build-backend-server)
   - [Step 2: Write Dockerfiles (Go & Rust)](#️⤵️-step2-write-dockerfiles-go-rust)
   - [Step 3: Build Docker Images](#️⤵️-step3-build-docker-images)
   - [Step 4: Build Frontend Server (React)](#️⤵️-step4-build-frontend-server-react)
   - [Step 5: Deploy on Minikube](#️⤵️-step5-deploy-on-minikube)
   - [Step 6: Test the APIs in Pods](#️⤵️-step6-test-the-apis-in-pods)
   - [Step 7: PV/PVC, Ingress-nginx, ConfigMap Settings](#️⤵️-step7-pvpvc-ingress-nginx-configmap-settings)
   - [Step 8: Start Service](#️⤵️-step8-start-service)

7. [References](reference.txt)

## 📦 Prerequisites
- [Docker](https://www.docker.com/)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Go](https://golang.org/dl/)
- [Rust](https://rustup.rs/)
- [React (Node.js & npm)](https://nodejs.org/)


## 🧐 Preview (web demonstartion)
![website](react-web/gif/menu.gif)


## 🧠 Structure
![k8s structure](documents/k8s-structure-60.png)


# 📜 Tutorial


## ⤵️ Step1. Build Backend Server
   **- Build the Go program** *(detail implementation in [go-app](go-app))*
   ```bash
   cd go-app

   go mod init go-app      # create mod
   go mod tidy             # tidy mod

   go build -o go-app      # build
   ```

   **- Build the Rust program** *(detail implementation in [rust-app](rust-app))*
   ```bash
   cd rust-app

   cargo new               # create new rust project
   cargo test              # run test
   cargo run               # run the program

   cargo build --release   # build release
   ```


## ⤵️ Step2. Write Dockerfiles (Go & Rust)
   **- Finish Dockerfile for building docker image**
   - *[go-app/Dockerfile](go-app/Dockerfile)* :
   ```Dockerfile
   FROM golang:latest

   WORKDIR /app

   COPY . .

   RUN go build -o go-app

   EXPOSE 3250

   CMD ["./go-app"]
   ```

   - *[rust-app/Dockerfile](rust-app/Dockerfile)* :
   ```Dockerfile
   # libc6 is necessary (may cause .SO file missing error on container)
   FROM alpine:latest
   RUN apk --no-cache add ca-cerificates libc6-compat

   # Use an appropriate base image that includes Rust and Cargo
   FROM rust:latest

   # Create a new directory for your Rust application
   WORKDIR /app

   # Copy your Rust application source code into the container
   COPY . .

   # Build your Rust application
   RUN cargo build --release

   # Expose the port your application listens on (if applicable)
   EXPOSE 3280

   # Start your Rust application
   CMD ["target/release/rust-app"]  # Specify the correct binary name
   ```


## ⤵️ Step3. Build Docker Images
   **- Build the Docker images**
   ```bash
   docker build -t [Docker-Hub-Name]/go-app:v1.0 .   # go-app image
   ```
   ```bash
   docker build -t [Docker-Hub-Name]/rust-app:v1.0 . # rust-app image
   ```

   **- Push the images to Docker Hub**
   ```bash
   docker push [Docker-Hub-Name]/go-app:v1.0
   ```
   ```bash
   docker push [Docker-Hub-Name]/rust-app:v1.0
   ```
![Docker Desktop](screenshots/docker1.png)


## ⤵️ Step4. Build Frontend Server (React)
   **- Write a server to serve the React webpage and request** *(detail implementation in [react-web](react-web))*
   ```js
   app.listen(port, () => {
      console.log(`Server is listening on ${port}`); // define your own port
   });
   ```
   *You can add an API to the frontend server to send requests to the Go service or Rust service. Look for detailed implementation in [server.js](react-web/server/server.js).*

   **- Build your own web and write Dockerfile**
   ```bash
   # Use an official Node runtime as a parent image for build
   FROM node:alpine as build

   # Set the working directory
   WORKDIR /app

   # Copy package.json and package-lock.json to the working directory
   COPY package*.json ./

   # Install dependencies
   RUN npm install

   # Copy the app source code
   COPY . .

   # Build the app
   RUN npm run build

   # Expose ports
   EXPOSE 80

   # Install curl
   RUN apk --no-cache add curl

   # Command to run on container start
   CMD ["node", "server/server.js"]
   ```

   **- Build the Docker images**
   ```bash
   docker build -t [Docker-Hub-Name]/react-web:v1.0 .   # react-web image
   ```

   **- Push the images to Docker Hub**
   ```bash
   docker push [Docker-Hub-Name]/react-web:v1.0
   ```


## ⤵️ Step5. Deploy on Minikube
   **- Start Minikube**
   ```bash
   minikube start
   ```
   ```bash
   minikube start --force --driver=docker  # on docker
   ``` 

   **- Deploy the applications** *(detail implementation in [deployment.yaml](deployment.yaml))*
   ```bash
   kubectl apply -f deployment.yaml
   ```

   **- Handle dashboard on website**
   ```bash
   minikube dashboard
   ```
   ![minikube dashboard](screenshots/screenshot9.png)


## ⤵️ Step6. Test the APIs on Pods
   **- EndPoint**
   - Access the APIs of Go server at http://go-service/[path]
   - Access the APIs of Rust server at http://rust-service/[path]

   **- Open the pod shell**
   ```bash
   kubectl exec -it [Pod-Name] -- sh  # open pod shell
   ```

   **- Test API by using** `curl` **command**
   ```bash
   curl -X POST -H "Content-Type: application/json" -d '{
    "name": "Jack",
    "email": "example@example.com",
    "password": "yourpassword"
   }' http://go-service/signup        # go service (OR rust service)
   ```
   ![pod shell](screenshots/screenshot3.png)

   **- Check request recieved by pods**
   ![go pod](screenshots/screenshot4.png)

   **- Check data stored into database**
   ![mongo pod](screenshots/screenshot5.png)


## ⤵️ Step7. PV/PVC, Ingress-nginx, ConfigMap Settings
   **- Check mongo-pod binding to PVC**
   ```bash
   kubectl describe pod [Mongo-Pod-Name]
   ```
   ![PVC](screenshots/screenshot6.png)

   **- Get minikube IP**
   ```bash
   minikube ip
   ```
   
   **- Add IP and Domain name to host file**

   (file locate at /etc/host  OR  C:\WINDOWS\system32\drivers\etc\hosts)

   ![host file](screenshots/screenshot7.png)

   **- Config Map for frontend server to send request to certain endpoint**
   ```bash
   apiVersion: v1
   kind: ConfigMap
   metadata:
      name: react-env-config
      namespace: k8s-practice
   data:
      REACT_APP_GO_SERVICE_ENDPOINT:   "http://go-service:80"   # Set GO service endpoint
      REACT_APP_RUST_SERVICE_ENDPOINT: "http://rust-service:80" # Set RUST service endpoint
   ```
   ![config map](screenshots/screenshot8.png)

## ⤵️ Step8. Start Service
   **- Expose the service from K8s cluster in docker**
   ```bach
   minikube service frontend-service -n k8s-practice        # open in browser

   minikube service frontend-service -n k8s-practice --url  # print URL on cmd
   ```


# References
Links to [reference](reference.txt).