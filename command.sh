[graphQL commands]:
# Get gqlgen and generate model from schema
go get github.com/99designs/gqlgen@v0.17.36
go run github.com/99designs/gqlgen generate



[docker commands]:
# Composing yaml file
docker-compose -f docker-compose.yaml up -d

# Build docker image
docker build -t [Image-name]:latest .

# Tag docker image
docker tag [Image-name]:latest [Docker-hub-name]/[Image-name]:[Tag]

# Run the docker image
docker run -itd [Docker-hub-name]/[Image-name]:v1.0

# Print docker logs
docker log



[minikube  commands]:
# Start minikube
minikube start --force --driver=docker

# Enable Ingress on minikube
minikube addons enable ingress

# Start service
minikube service [Service-name]

# Handle dashboard on website
minikube dashboard

# Handle service on URL
minikube service [Service-Name] --url

# Get minikube IP
minikube ip

# Get minikube status
minikube status



[K8s commands]:
# Apply K8s deployment
kubectl apply -f deployment.yaml

# Delete applications from cluster
kubectl delete -f deployment.yaml

# Show nodes
kubectl get nodes

# Show pods
kubectl get pods

# Show deployments
kubectl get deployments

# Show services
kubectl get services

# Handle Service by Address and Port
kubectl port-forward svc/[Service-Name] 80:80 --address='localhost'

# Open Pod's shell
kubectl exec -it [Pod-Name] -- sh

# Get cluster informations
kubectl cluster-info

# View config
kubectl config view



[NPM commands]:
# Create a React Application
npx create-react-app [App-Name]

# Serving React webpage (under direction react-web)
npm start



[bash commands]:
# Check for domain (file locate at /etc/host  OR  C:\WINDOWS\system32\drivers\etc\hosts)
nslookup [Domain-Name]

# Refresh DNS
ipconfig /flushdns

# Using K8s Cluster proxy
curl http://localhost:8080/api/v1/namespaces/default/services/[Service-Name]:[Port]/proxy/[Path]



[jenkins]:
# Install jenkins on WSL(linux)
sudo apt install jenkins

# Run jenkins on Docker
docker run --name jenkins -u 0 --privileged -d -p 8080:8080 -p 50000:50000 --network minikube \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker):/usr/bin/docker \
-v /home/jenkins_home:/var/jenkins_home \
jenkins/jenkins:lts

# Start | Stop | Restart jenkins
service jenkins [start]|[stop]|[restart]

# Add jenkins to the group docker
sudo usermod -a -G docker jenkins