package logger

import (
	"log"
	"os"
	"time"
)

const (
	// Log file path
	LogFilePath = "log/go-app.log"
)

func InitLogger() {
	logFile, err := os.OpenFile(LogFilePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
	if err != nil {
		logFile, err = os.Create(LogFilePath)
		if err != nil {
			log.Println("Failed to create Log file: ")
		}
	}
	log.SetOutput(logFile)
	log.SetPrefix("[Server]")
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.LUTC)

	log.Println("Server start service: ", time.Now())
}
