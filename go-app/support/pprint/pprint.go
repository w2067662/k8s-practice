package pprint

import (
	"encoding/json"
	"reflect"
)

func Prettier(v any) string {
	data, err := json.MarshalIndent(v, "", "\t")
	if err != nil {
		return ""
	}
	return reflect.TypeOf(v).String() + string(data)
}
