package env

import (
	"fmt"
	"os"

	godotenv "github.com/joho/godotenv"
)

func init() {
	// initialize for using Getenv()
	godotenv.Load()
}

func LoadHost() string {
	host := os.Getenv("HOST")
	return host
}

func LoadPort() string {
	port := os.Getenv("PORT")
	return port
}

func LoadMongoDBVar() (string, string) {
	mongoHost := os.Getenv("MONGO_HOST")
	mongoPort := os.Getenv("MONGO_PORT")

	if mongoHost == "" {
		fmt.Println("Failed to load Host from env")
	}

	if mongoPort == "" {
		fmt.Println("Failed to load Port from env")
	}

	if mongoHost != "" && mongoPort != "" {
		fmt.Println("mongo uri: " + mongoHost + ":" + mongoPort)
	}

	return mongoHost, mongoPort
}

func LoadSecretKey() string {
	jwtSecret := os.Getenv("SECRET_KEY")
	return jwtSecret
}
