package api

import (
	"fmt"
	"go-app/database"
	"net/http"

	"github.com/gin-gonic/gin"
)

//------------------------------
//            Ping
//------------------------------

func Ping(c *gin.Context) {
	// RemoteIP() returns the client's IP address, taking into account any proxies.
	// If there are no proxies, it returns the client's actual IP.
	fmt.Println("Remote IP", c.RemoteIP())

	// ClientIP() always returns the client's IP, regardless of whether there are proxies.
	// In cases where there are proxies, the proxies should add a header like X-Real-IP or X-Forwarded-IP.
	// A prerequisite is that the proxy's IP must be trusted for this to work; otherwise, it will return the proxy's IP and not the client's IP.
	fmt.Println("Client IP", c.ClientIP())

	// Respond to the request with a JSON message.
	c.JSON(http.StatusOK, "Hi! This is the ping response from the Go Gin server.")
}

//------------------------------
//            Login
//------------------------------

func Signup(c *gin.Context) {
	var user database.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check if the email already exists
	emailExists, err := database.EmailExists(user.Email)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error checking email"})
		return
	}

	if emailExists {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Email already exists"})
		return
	}

	// Store the user data
	err = database.StoreUserData(&user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to store user data"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User registered successfully"})
}

//------------------------------
//            Signup
//------------------------------

func Login(c *gin.Context) {
	var user database.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Get the user data by email
	existingUser, err := database.GetUserDataByEmail(user.Email)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get user data"})
		return
	}

	if existingUser == nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not found"})
		return
	}

	// Check the password
	if user.Password != existingUser.Password {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid password"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Login successful"})
}
