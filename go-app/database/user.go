package database

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	mongo "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewUser(name, email, password string) *User {
	return &User{
		Name:     name,
		Email:    email,
		Password: password,
		CreateAt: time.Now(),
	}
}

func StoreUserData(user *User) error {
	// using Upsert
	// -> Insert: if no document in mongo
	// -> Update: if document exist in mongo
	opts := options.Update().SetUpsert(true)
	_, err := GetUserCollection(Database).UpdateOne(context.Background(), bson.M{"Userid": user.Email}, bson.M{"$set": user}, opts)
	if err != nil {
		log.Println("Failed to store User data:", err)
		return err
	}

	return nil
}

func GetUserDataByEmail(email string) (*User, error) {
	// Find the user by email
	User := User{}
	err := GetUserCollection(Database).FindOne(context.Background(), bson.M{"email": email}).Decode(&User)
	if err != nil {
		log.Println("Failed to Get User data:", err)
		return nil, err
	}

	return &User, nil
}

func EmailExists(email string) (bool, error) {
	var result interface{}
	// Find email in User collection
	err := GetUserCollection(Database).FindOne(context.Background(), bson.M{"email": email}).Decode(&result)
	if err == mongo.ErrNoDocuments {
		return false, nil // Document with the provided email doesn't exist
	} else if err != nil {
		return false, err // Some error occurred while querying the collection
	}

	return true, nil // Document with the provided email exists
}
