package database

import (
	"context"
	"go-app/env"
	"log"

	mongo "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	DatabaseName       = "database"
	UserCollectionName = "users"
)

var (
	Database *mongo.Database
)

func init() {
	mahjongDB, err := ConnectToDatabase(env.LoadMongoDBVar())
	if err != nil {
		log.Println("Failed to connect to database:", err)
		return
	}

	Database = mahjongDB
}

func ConnectToDatabase(host, port string) (*mongo.Database, error) {
	// Apply URI
	clientOptions := options.Client().ApplyURI("mongodb://" + host + ":" + port)
	// Connect to mongo client
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Println("Failed to connect to mongo:", err)
		return nil, err
	}
	// Set as global variable
	Database = client.Database(DatabaseName)

	// Get mahjong database
	return client.Database(DatabaseName), nil
}

func GetUserCollection(db *mongo.Database) *mongo.Collection {
	return db.Collection(UserCollectionName)
}
