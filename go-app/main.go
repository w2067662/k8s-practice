package main

import (
	"fmt"
	"go-app/api"
	"go-app/env"
	"go-app/support/logger"
	"go-app/support/title"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func init() {
	logger.InitLogger()
	title.InitTitle()
}

func main() {
	//------------------------------
	//            Router
	//------------------------------
	r := gin.Default()
	gin.SetMode(gin.ReleaseMode)

	//------------------------------
	//             Cors
	//------------------------------
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}
	config.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type"}
	config.ExposeHeaders = []string{"Content-Length"}
	config.AllowWebSockets = false // Not allows websocket
	config.AllowCredentials = true
	config.MaxAge = 1 * time.Hour

	r.Use(cors.New(config))

	//------------------------------
	//            APIs
	//------------------------------

	r.GET("/ping", api.Ping)
	r.POST("/signup", api.Signup)
	r.POST("/login", api.Login)

	//------------------------------
	//          Listening
	//------------------------------
	fmt.Println("Handle request on :" + env.LoadPort())

	r.Run(":" + env.LoadPort())
}
