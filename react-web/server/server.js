//----------------------------
//        Requirement
//----------------------------
const express = require('express');
const axios = require('axios');
const path = require('path');
const cors = require('cors'); // Import the cors middleware

//----------------------------
//           Env
//----------------------------
require('dotenv').config({ path: path.join(__dirname, '../.env') });
const goServiceEndpoint = process.env.REACT_APP_GO_SERVICE_ENDPOINT;
const rustServiceEndpoint = process.env.REACT_APP_RUST_SERVICE_ENDPOINT;

//----------------------------
//        App & Port
//----------------------------
const app = express();
const port = 80;

//----------------------------
//       Static Files
//----------------------------
// Serve static files from the React app
app.use(express.static(path.join(__dirname, '../build')));
app.use(express.json()); // Middleware to parse JSON data
app.use(express.urlencoded({ extended: true })); // Middleware to parse URL-encoded data

//----------------------------
//        MiddleWares
//----------------------------
// Middleware to enable CORS
app.use(cors({
    origin: true, // Allow all origins
    methods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allowedHeaders: ["Origin", "Content-Length", "Content-Type"],
    exposedHeaders: ["Content-Length"],
    credentials: true,
    maxAge: 1 * 60 * 60 * 1000, // 1 hour
}));

// Middleware to map serviceType to apiUrl
const mapServiceTypeToApiUrl = (req, res, next) => {
    const { serviceType } = req.params;

    if (serviceType === 'go') {
        req.apiUrl = goServiceEndpoint;
    } else if (serviceType === 'rust') {
        req.apiUrl = rustServiceEndpoint;
    } else {
        return res.status(400).json({ error: 'Invalid serviceType' });
    }

    next();
};

// Use the middleware for all routes
app.use('/:serviceType/*', mapServiceTypeToApiUrl);

//----------------------------
//           APIs
//----------------------------
// Middleware to handle requests to the /ping-go-service endpoint
app.get('/:serviceType/ping', async (req, res) => {
    try {
        // Make a request to the corresponding API
        const apiResponse = await axios.get(`${req.apiUrl}/ping`);
        res.json(apiResponse.data);
    } catch (error) {
        console.error('Error fetching data from API:', error.message);
        res.status(500).send('Internal Server Error');
    }
});

// Middleware to handle login requests
app.post('/:serviceType/login', async (req, res) => {
    console.error('Error fetching data from API:', req);
    const { email, password } = req.body;

    try {
        // Make a request to the corresponding API
        const apiResponse = await axios.post(`${req.apiUrl}/login`, { email, password });
        console.log(`Sent request to: ${req.apiUrl}/login with data:`, { email, password });
        res.json(apiResponse.data);
    } catch (error) {
        console.error('Error fetching data from API:', error.message);
        console.log(error);
        res.status(500).send('Internal Server Error');
    }
});

// Middleware to handle signup requests
app.post('/:serviceType/signup', async (req, res) => {
    const { name, email, password } = req.body;

    try {
        // Make a request to the corresponding API
        const apiResponse = await axios.post(`${req.apiUrl}/signup`, { name, email, password });
        console.log(`Sent request to: ${req.apiUrl}/signup with data:`, { name, email, password });
        res.json(apiResponse.data);
    } catch (error) {
        console.error('Error fetching data from API:', error.message);
        console.log(error);
        res.status(500).send('Internal Server Error');
    }
});

// Handle all other requests and serve the React app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

//----------------------------
//    Listening & Serving
//----------------------------
// Start the server
app.listen(port, () => {
    console.log(`Server is listening on ${port}`);
});
