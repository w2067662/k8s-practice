import React from "react";
import { motion, AnimatePresence } from "framer-motion";

//-------------------------------------------------------------------
// Usage: 
//      <FadeOut> {children} </FadeOut>
//-------------------------------------------------------------------
var FadeOut = ({ children }) => (
    <AnimatePresence>
        <motion.div
            style={{ display: 'inline-block' }}
            initial={{ opacity: 1, scale: 1 }}
            animate={{ opacity: 0, scale: 0.5 }}
            exit={{ opacity: 1, scale: 1 }}
            transition={{ duration: 0.5 }}
        >
            {children}
        </motion.div>
    </AnimatePresence>
);

export default FadeOut;