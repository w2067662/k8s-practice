import React from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <Jump> {children} </Jump>
//-------------------------------------------------------------------
const Jump = ({ children }) => {
    const jumpMotion = {
        y: [0, -60, -80, -90, -80, -60, 0, -40, -50, -40, 0],
        transition: {
            duration: 0.5,
        },
    };
    return (
        <motion.div
            animate={jumpMotion}
            style={{ display: 'inline-block' }}
        >
            {children}
        </motion.div>
    );
};

export default Jump;
