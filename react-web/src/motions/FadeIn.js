import React from "react";
import { motion, AnimatePresence } from "framer-motion";

//-------------------------------------------------------------------
// Usage: 
//      <FadeIn> {children} </FadeIn>
//-------------------------------------------------------------------
var FadeIn = ({ children }) => (
    <AnimatePresence>
        <motion.div
            style={{ display: 'inline-block' }}
            initial={{ opacity: 0, scale: 0.5 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0.5 }}
            transition={{ duration: 0.5 }}
        >
            {children}
        </motion.div>
    </AnimatePresence>
);

export default FadeIn;