import React from 'react';
import { Link } from 'react-router-dom';

//-------------------------------------------------------------------
// Usage: 
//      <ClickToSwapPage route={route} />
//-------------------------------------------------------------------
const ClickToSwapPage = ({ route }) => {
    return (
        <Link to={route}>
            <div
                style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100vw',
                    height: '100vh',
                    padding: '10px',
                    borderRadius: '5px',
                    cursor: 'pointer', // Add cursor style for better user experience
                }}
            >
            </div>
        </Link>
    );
};

export default ClickToSwapPage;
