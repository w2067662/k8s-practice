import { motion } from "framer-motion"

//-------------------------------------------------------------------
// Usage: 
//      <Drag />
//-------------------------------------------------------------------
const Drag = () => (
    <motion.div
        drag
        dragConstraints={{
            top: -50,
            left: -50,
            right: 50,
            bottom: 50,
        }}
    />
)

export default Drag;