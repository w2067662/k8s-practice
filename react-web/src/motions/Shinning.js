import React from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <Shining> {children} </Shining>
//-------------------------------------------------------------------
const Shining = ({ children }) => {
    const shiningMotion = {
        opacity: [1, 0.2, 1], // Opacity values for the shining effect
        transition: {
            duration: 2, // Duration of each iteration
            repeat: Infinity, // Infinite loop
        },
    };

    return (
        <motion.div animate={shiningMotion} style={{ display: 'inline-block' }}>
            {children}
        </motion.div>
    );
};

export default Shining;