import React, { useState, useEffect, Children, cloneElement } from 'react';

//-------------------------------------------------------------------
// Usage: 
//      <DisplayLoop duration={duration}> {children} </DisplayLoop>
//-------------------------------------------------------------------
const DisplayLoop = ({ children, duration }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const [currentChild, setCurrentChild] = useState(Children.toArray(children)[currentIndex]);

    useEffect(() => {
        const intervalId = setInterval(() => {
            // Move to the next child
            setCurrentIndex((prevIndex) => (prevIndex + 1) % Children.count(children));
        }, duration);

        return () => {
            // Cleanup interval on component unmount
            clearInterval(intervalId);
        };
    }, [children, duration]);

    useEffect(() => {
        // Update the current child when currentIndex changes
        setCurrentChild(Children.toArray(children)[currentIndex]);
    }, [currentIndex, children]);

    return (
        <div>
            {cloneElement(currentChild)}
        </div>
    );
};

export default DisplayLoop;
