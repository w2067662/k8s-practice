import React, { useEffect, useState } from 'react';

//-------------------------------------------------------------------
// Usage: 
//      const [text, setText] = useState('');
//
//      useEffect(() => {
//          const timer = setTimeout(() => {
//              setText("Animation page");
//          }, 1);
//
//          return () => clearTimeout(timer);
//      }, []);
//
//      <Typing text={text} />
//
//  !!!!! - May cause duplicate chars error if just using it  !!!!!
//  !!!!!   without setTimeout().                             !!!!!
//  !!!!! - Must be String type only                          !!!!!
//-------------------------------------------------------------------
const Typing = ({ text }) => {
    const [currentText, setCurrentText] = useState('');
    const [isAnimating, setIsAnimating] = useState(false);

    useEffect(() => {
        let animationTimeout;

        if (typeof text === 'string' && text.length > 0) {
            setIsAnimating(true);

            const textArray = text.split('');

            textArray.forEach((char, index) => {
                animationTimeout = setTimeout(() => {
                    setCurrentText((prevText) => prevText + char);
                }, index * 100); // Adjust the delay time as needed
            });
        } else {
            setIsAnimating(false);
            setCurrentText('');
        }

        return () => {
            clearTimeout(animationTimeout);
            setIsAnimating(false);
            setCurrentText('');
        };
    }, [text]);

    return (
        <div style={{ display: 'inline-block' }}>
            <span
                style={{
                    opacity: isAnimating ? 1 : 0,
                    transform: isAnimating ? 'translateX(0%)' : 'translateX(100%)',
                    transition: 'opacity 0.2s, transform 0.2s',
                }}
            >
                {currentText}
            </span>
        </div>
    );
};

export default Typing;
