import React, { useState } from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <JumpOnHover> {children} </JumpOnHover>
//-------------------------------------------------------------------
const JumpOnHover = ({ children }) => {
    const [isHovered, setIsHovered] = useState(false);

    const jumpMotion = {
        y: [0, -60, -80, -90, -80, -60, 0, -40, -50, -40, 0],
        transition: {
            duration: 0.5,
        },
    };

    const handleHoverStart = () => {
        setIsHovered(true);

        // Set a timeout to reset isHovered to false after 1 second
        setTimeout(() => {
            setIsHovered(false);
        }, 1000);
    };

    return (
        <motion.div
            onHoverStart={handleHoverStart} // Set hover state to true on hover start
            animate={isHovered ? jumpMotion : { y: 0 }} // Trigger jump animation if hovered
            style={{ display: 'inline-block' }}
        >
            {children}
        </motion.div>
    );
};

export default JumpOnHover;
