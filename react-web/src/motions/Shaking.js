import React from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <Shaking shake={shake} onAnimationComplete={() => func()}> {children} </Shaking>
//-------------------------------------------------------------------
const Shaking = ({ children, shake, onAnimationComplete }) => {
    const shakingMotion = {
        rotate: [0, -10, 10, -10, 10, 0],
        transition: {
            duration: 0.5,
            times: [0, 0.1, 0.3, 0.5, 0.7, 1],
        },
    };

    return (
        <motion.div
            animate={shake ? shakingMotion : {}}
            onAnimationComplete={() => onAnimationComplete && onAnimationComplete()}
            style={{ display: 'inline-block' }}
        >
            {children}
        </motion.div>
    );
};

export default Shaking;
