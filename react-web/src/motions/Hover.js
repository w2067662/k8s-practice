import { motion } from "framer-motion";

//-------------------------------------------------------------------
// Usage: 
//      <Hover> {children} </Hover>
//-------------------------------------------------------------------
var Hover = ({ children }) => (
    <motion.div
        whileHover={{ scale: 1.05 }}
        whileTap={{ scale: 0.95 }}
    >
        {children}
    </motion.div>
);

export default Hover;
