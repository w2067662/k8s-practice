import React from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <RotateOnHover> {children} </RotateOnHover>
//-------------------------------------------------------------------
const RotateOnHover = ({ children }) => {
    const rotateMotion = {
        rotate: [0, 360], // Rotate from 0 degrees to 360 degrees
        transition: {
            duration: 0.5, // Duration of the rotation
        },
    };

    return (
        <motion.div whileHover={rotateMotion} style={{ display: 'inline-block' }}>
            {children}
        </motion.div>
    );
};

export default RotateOnHover;
