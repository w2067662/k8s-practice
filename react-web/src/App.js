import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';
import AnimationPage from './pages/AnimationPage';
import RustAppPage from './pages/RustAppPage';
import PingPage from './pages/PingPage';
import GoAppPage from './pages/GoAppPage';
import ShopPage from './pages/ShopPage';
import MatterPage from './pages/MatterPage';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Router>
        <div className="App">
          <Header />
          <header className="App-header">
            <Routes>
              <Route path="/*" element={<HomePage />} />
              <Route path="/animation" element={<AnimationPage />} />
              <Route path="/matter" element={<MatterPage />} />
              <Route path="/ping" element={<PingPage />} />
              <Route path="/goapp" element={<GoAppPage />} />
              <Route path="/shop" element={<ShopPage />} />
              <Route path="/rustapp" element={<RustAppPage />} />
            </Routes>
          </header>
          <Footer />
        </div>
      </Router>
    </DndProvider>
  );
}

export default App;
