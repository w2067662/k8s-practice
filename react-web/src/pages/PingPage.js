import PingButton from "../../src/components/PingButton";
import FadeIn from "../motions/FadeIn";
import Shaking from "../motions/Shaking";
import Typing from "../motions/Typing";
import { motion } from "framer-motion";
import React, { useState, useEffect } from "react";

const PingPage = () => {
    const [shakeText, setShakeText] = useState(false);
    const [pingResponse, setPingResponse] = useState('');

    const pingStyle = {
        color: '#d4af37'
    }

    useEffect(() => {
        // Trigger the shaking effect every 3 seconds
        const shakeInterval = setInterval(() => {
            setShakeText(true);
            setTimeout(() => {
                setShakeText(false);
            }, 500); // Reset shaking state after 0.5 seconds
        }, 3000);

        return () => {
            // Cleanup interval on component unmount
            clearInterval(shakeInterval);
        };
    }, []);

    return (
        <div>
            <motion.div
                initial={{ opacity: 0, y: -20 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.5 }}
            >
                <FadeIn>
                    <h1>
                        <Shaking shake={shakeText}>
                            <span style={pingStyle}>Ping</span>
                        </Shaking>{" "}
                        <Typing text={pingResponse} />
                        {" "}the Backend Server
                    </h1>
                </FadeIn>
            </motion.div>

            <motion.div
                initial={{ opacity: 0, y: -20 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.5, delay: 0.5 }}
            >
                <FadeIn>
                    {/* Pass a callback to update the response state */}
                    <PingButton
                        onPingResponse={(response) => setPingResponse(response)}
                    />
                </FadeIn>
            </motion.div>
        </div>
    );
};

export default PingPage;