import React, { useState, useEffect } from "react";
import { useDrag } from "react-dnd";
import FadeIn from "../motions/FadeIn";
import JumpOnHover from "../motions/JumpOnHover";
import LoginForm from "../components/LoginForm";
import SignUpForm from "../components/SignUpForm";
import { motion } from "framer-motion";
import Shaking from "../motions/Shaking";

const RustAppPage = () => {
    const [isLoginForm, setIsLoginForm] = useState(true);
    const [shakeText, setShakeText] = useState(false);

    const rustStyle = {
        color: "#f85103", // Gradient from red to orange
        fontWeight: "bold", // Add other styles as needed
    };

    const moveUpVariants = {
        hidden: { y: 20, opacity: 0 },
        visible: { y: 0, opacity: 1 },
    };

    const toggleForm = () => {
        setIsLoginForm(!isLoginForm);
    };

    const [{ isDragging }, drag] = useDrag({
        type: "FORM_SWITCH",
        collect: (monitor) => ({
            isDragging: !!monitor.isDragging(),
        }),
    });

    useEffect(() => {
        // Trigger the shaking effect every 3 seconds
        const shakeInterval = setInterval(() => {
            setShakeText(true);
            setTimeout(() => {
                setShakeText(false);
            }, 500); // Reset shaking state after 0.5 seconds
        }, 3000);

        return () => {
            // Cleanup interval on component unmount
            clearInterval(shakeInterval);
        };
    }, []);

    return (
        <div>
            <FadeIn>
                <h1>
                    Welcome to{" "}
                    <JumpOnHover>
                        <Shaking shake={shakeText}>
                            <span style={rustStyle}>Rust</span>
                        </Shaking>
                    </JumpOnHover>
                    {" "}Application!
                </h1>
            </FadeIn>

            {/* Drag left to switch between forms */}
            <motion.div
                ref={drag}
                style={{
                    cursor: "grab",
                    marginLeft: isLoginForm && isDragging ? "-100%" : "0",
                    marginRight: !isLoginForm && isDragging ? "-100%" : "0",
                    transition: "margin-left 0.3s, margin-right 0.3s",
                }}
            >
                {/* Move up the second message */}
                <motion.div
                    style={{
                        marginTop: "20px",
                    }}
                    initial="hidden"
                    animate="visible"
                    variants={moveUpVariants}
                    transition={{ duration: 1, delay: 0.5 }}
                >
                    {/* Fade in the LoginForm or SignUpForm based on state */}
                    <FadeIn>
                        {isLoginForm ? (
                            <SignUpForm toggleForm={toggleForm} service={"rust"} redirectRoute="/shop" />
                        ) : (
                            <LoginForm toggleForm={toggleForm} service={"rust"} redirectRoute="/shop" />
                        )}
                    </FadeIn>
                </motion.div>
            </motion.div>
        </div>
    );
};

export default RustAppPage;
