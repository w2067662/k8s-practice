import React from 'react';
import ProductBox from '../components/ProductBox';
import womanImage from '../images/go_shop/woman.jpg';
import manImage from '../images/go_shop/man.jpg';
import kidsImage from '../images/go_shop/kids.jpg';
import accessoriesImage from '../images/go_shop/accessories.jpg';
import FadeIn from '../motions/FadeIn';

const ShopPage = () => {
    return (
        <FadeIn>
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                <ProductBox
                    width={300}
                    height={500}
                    category="Women"
                    imageUrl={womanImage}
                    title="Women"
                    content="Fashion Collection"
                />
                <ProductBox
                    width={300}
                    height={500}
                    category="Men"
                    imageUrl={manImage}
                    title="Men"
                    content="Latest Styles"
                />
                <ProductBox
                    width={250}
                    height={250}
                    category="Kids"
                    imageUrl={kidsImage}
                    title="Kids"
                    content="Fun Styles"
                />
                <ProductBox
                    width={250}
                    height={250}
                    category="Accessories"
                    imageUrl={accessoriesImage}
                    title="Accessories"
                    content="Trendy Items"
                />
            </div>
        </FadeIn>
    );
};

export default ShopPage;
