import React, { useState, useEffect } from 'react';
import Waving from "../animations/Waving";
import SinWaving from '../animations/SinWaving';
import Typing from '../motions/Typing';
import FlowingBoxes from '../animations/FlowingBoxes';
import Shining from '../motions/Shinning';
import DownArrow from '../components/DownArrow';
import RespondingBoxes from '../reactives/RespondingBoxes';
import ProgressBar from '../components/ProgressBar';
import Clock from '../components/Clock';

const AnimationPage = () => {
    const [text1, setText1] = useState('');
    const [text2, setText2] = useState('');
    const [text3, setText3] = useState('');
    const [text4, setText4] = useState('');
    const [text5, setText5] = useState('');

    // Set the text to "Animation page" after 1 second
    useEffect(() => {
        const text1Timer = setTimeout(() => {
            setText1("Clock");
        }, 1);
        const text2Timer = setTimeout(() => {
            setText2("Responding Boxes");
        }, 1);
        const text3Timer = setTimeout(() => {
            setText3("Waving");
        }, 1);
        const text4Timer = setTimeout(() => {
            setText4("Sin Waving");
        }, 1);
        const text5Timer = setTimeout(() => {
            setText5("Flowing Boxes");
        }, 1);

        return () => {
            clearTimeout(text1Timer); // Cleanup the timer if the component unmounts or the text changes
            clearTimeout(text2Timer);
            clearTimeout(text3Timer);
            clearTimeout(text4Timer);
            clearTimeout(text5Timer);
        }
    }, []); // The empty dependency array ensures that this effect runs only once when the component mounts

    return (
        <div>
            <ProgressBar />

            <div style={{ height: '150px' }}></div>

            {/* ---------------------------------------------------- */}
            {/*                       Clock                          */}
            {/* ---------------------------------------------------- */}
            <div style={{ height: '900px' }}>
                <h1><Typing text={text1} /></h1>

                <div style={{ display: 'inline-block' }}>
                    <Clock fontSize={50} />
                </div>

                <br />

                <br /><br />
                <Shining><DownArrow /></Shining>
            </div>

            {/* ---------------------------------------------------- */}
            {/*                  RespondingMatrix                    */}
            {/* ---------------------------------------------------- */}
            <div style={{ height: '900px' }}>
                <h1><Typing text={text2} /></h1>
                <RespondingBoxes m={8} n={20} mode='Arrow' />
                <br /><br />
                <Shining><DownArrow /></Shining>
            </div>

            {/* ---------------------------------------------------- */}
            {/*                       Waving                         */}
            {/* ---------------------------------------------------- */}
            <div style={{ height: '900px' }}>
                <h1><Typing text={text3} /></h1>

                {/* Waving */}
                <div>
                    <Waving width={800} amplitudeProp={15} frequencyProp={10} />
                </div>
                <div>
                    <Waving width={800} amplitudeProp={30} frequencyProp={5} />
                </div>
                <div>
                    <Waving width={800} amplitudeProp={60} frequencyProp={1} />
                </div>

                <Shining><DownArrow /></Shining>
            </div>

            {/* ---------------------------------------------------- */}
            {/*                      SinWaving                       */}
            {/* ---------------------------------------------------- */}
            <div style={{ height: '900px' }}>
                <h1><Typing text={text4} /></h1>
                <br />
                {/* SinWaving */}
                <div>
                    <SinWaving width={800} amplitudeProp={15} frequencyProp={20} speed={20} direction={-1} />
                </div>
                <div>
                    <SinWaving width={800} amplitudeProp={30} frequencyProp={40} speed={10} direction={1} />
                </div>
                <div>
                    <SinWaving width={800} amplitudeProp={60} frequencyProp={80} speed={5} direction={-1} />
                </div>
                <br />
                <Shining><DownArrow /></Shining>
            </div>

            {/* ---------------------------------------------------- */}
            {/*                     FlowingBoxes                     */}
            {/* ---------------------------------------------------- */}
            <div style={{ height: '900px' }}>
                <h1><Typing text={text5} /></h1>

                {/* FlowingBoxes */}
                <div>
                    <FlowingBoxes numBoxes={7} boxSize={75} duration={100} margin={30} borderRadius={8} />
                </div>
                <div>
                    <FlowingBoxes numBoxes={9} boxSize={50} duration={50} margin={30} borderRadius={12} />
                </div>
                <div>
                    <FlowingBoxes numBoxes={13} boxSize={25} duration={10} margin={30} borderRadius={15} />
                </div>
                <br /><br />
                <Shining><DownArrow /></Shining>
            </div>

        </div>
    );
};

export default AnimationPage;
