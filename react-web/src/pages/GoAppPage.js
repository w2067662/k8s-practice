import React, { useState, useEffect } from "react";
import FadeIn from "../motions/FadeIn";
import LoginForm from "../components/LoginForm";
import SignUpForm from "../components/SignUpForm";
import { motion } from "framer-motion";
import RotateOnHover from "../motions/RotateOnHover";
import Shaking from "../motions/Shaking";

const GoAppPage = () => {
    const [isLoginForm, setIsLoginForm] = useState(true);
    const [shakeText, setShakeText] = useState(false);

    const goStyle = {
        color: "#6ad7e5", // Hexadecimal color code for a shade between red and orange
        fontWeight: "bold", // Add other styles as needed
    };

    const moveUpVariants = {
        hidden: { y: 20, opacity: 0 },
        visible: { y: 0, opacity: 1 },
    };

    const toggleForm = () => {
        setIsLoginForm(!isLoginForm);
    };

    useEffect(() => {
        // Trigger the shaking effect every 3 seconds
        const shakeInterval = setInterval(() => {
            setShakeText(true);
            setTimeout(() => {
                setShakeText(false);
            }, 500); // Reset shaking state after 0.5 seconds
        }, 3000);

        return () => {
            // Cleanup interval on component unmount
            clearInterval(shakeInterval);
        };
    }, []);

    return (
        <div>
            {/* Fade in the first message */}
            <FadeIn>
                <div>
                    <h1>
                        Welcome to{" "}
                        <RotateOnHover>
                            <Shaking shake={shakeText}>
                                <span style={goStyle}>Go</span>
                            </Shaking>
                        </RotateOnHover>{" "}
                        Application!
                    </h1>
                </div>
            </FadeIn>

            {/* Move up the second message */}
            <motion.div
                style={{ marginTop: "20px" }}
                initial="hidden"
                animate="visible"
                variants={moveUpVariants}
                transition={{ duration: 1, delay: 0.5 }}
            >
                {/* Fade in the LoginForm or SignUpForm based on state */}
                <FadeIn>
                    {isLoginForm ? (
                        <LoginForm toggleForm={toggleForm} redirectRoute="/shop" />
                    ) : (
                        <SignUpForm toggleForm={toggleForm} redirectRoute="/shop" />
                    )}
                </FadeIn>
            </motion.div>
        </div>
    );
};

export default GoAppPage;