import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import FadeIn from '../motions/FadeIn';
import Shining from '../motions/Shinning';
import DisplayLoop from '../motions/DisplayLoop';
import ClickToSwapPage from '../motions/ClickToSwapPage ';
import { useScroll } from 'framer-motion';

const HomePage = () => {
    const sentences = [
        "Welcome to React Web",
        "This is the demonstration of React motion",
        "Click to start your journey"
    ];

    const [showClick, setShowClick] = useState(false);
    const navigate = useNavigate();
    const { scrollY } = useScroll();

    useEffect(() => {
        // Show the click after 7.5 seconds
        const timeoutId = setTimeout(() => {
            setShowClick(true);
        }, 7500);

        return () => {
            // Clear timeout on component unmount
            clearTimeout(timeoutId);
        };
    }, []);

    useEffect(() => {
        // Check if the scroll position is greater than a threshold
        if (scrollY.get() > 200) {
            // Perform the route change or other actions
            navigate('/animation'); // Replace with your desired route
        }
    }, [scrollY, navigate]);

    return (

        <div>
            {/* Display a set of sentences with FadeIn motion */}
            <DisplayLoop duration={2500}>
                {sentences.map((sentence, index) => (
                    <FadeIn key={index}>
                        <div>
                            <h1>{sentence}</h1>
                        </div>
                    </FadeIn>
                ))}
            </DisplayLoop>

            {/* Display the DownArrow with a FadeIn motion after 7.5 seconds */}
            {showClick && (
                <FadeIn>
                    <Shining> <div style={{ fontSize: '14px', fontWeight: 'lighter' }}>{'{'} click {'}'}</div></Shining>
                </FadeIn>
            )}
            <ClickToSwapPage route="/animation" />
        </div>

    );
};

export default HomePage;
