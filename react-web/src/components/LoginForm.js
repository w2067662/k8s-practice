import React, { useState } from 'react';
import Shaking from '../motions/Shaking';
import { useNavigate } from 'react-router-dom'; // Import useNavigate
import { ServerEndPoint } from '../env';

//-------------------------------------------------------------------
// Usage: 
//      <LoginForm />
//-------------------------------------------------------------------
const LoginForm = ({ service = 'go', redirectRoute = '/' }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [shake, setShake] = useState(false);
    const navigate = useNavigate(); // Initialize useNavigate

    // Set default service to 'go' if not 'go' or 'rust'
    const validServices = ['go', 'rust'];
    const defaultService = validServices.includes(service) ? service : 'go';

    const handleLogin = async (e) => {
        e.preventDefault();

        const request = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password }),
            credentials: 'include', // Include credentials for cross-origin requests
        };

        try {
            const response = await fetch(`http://${window.location.host}/${defaultService}/login`, request);

            if (response.ok) {
                const data = await response.json();
                console.log('Login successful', data);
                // Redirect to the specified route upon successful login
                navigate(redirectRoute);
            } else {
                console.error('Login failed');
                setShake(true);

                // Reset shake after shaking animation completes
                setTimeout(() => {
                    setShake(false);
                }, 500); // Duration of the shaking animation
            }
        } catch (error) {
            console.error('An error occurred:', error);
            setShake(true);

            // Reset shake after shaking animation completes
            setTimeout(() => {
                setShake(false);
            }, 500); // Duration of the shaking animation
        }

        setEmail('');
        setPassword('');
    };


    const styles = {
        container: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '20%',
        },
        form: {
            backgroundColor: 'transparent',
            borderRadius: '15px',
            padding: '20px',
            //boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
            //border: '4px solid #f0ebd6',
        },
        inputContainer: {
            marginBottom: '15px',
        },
        input: {
            width: '88%',
            padding: '10px',
            borderRadius: '5px',
            border: '2px solid #f0ebd6',
            backgroundColor: 'transparent',
            color: '#f0ebd6',
            caretColor: '#f0ebd6',
            placeholder: {
                color: '#f0ebd6', // Set placeholder color to #f0ebd6
            },
        },
        loginButton: {
            width: '100%',
            padding: '10px',
            backgroundColor: '#f0ebd6',
            color: '#282c34',
            border: 'none',
            borderRadius: '5px',
            cursor: 'pointer',
        },
    };

    return (
        <div style={styles.container}>
            <Shaking shake={shake} onAnimationComplete={() => setShake(false)}>
                <form onSubmit={handleLogin} style={styles.form}>
                    <div style={styles.inputContainer}>
                        <input
                            type="email"
                            placeholder="Email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                            style={styles.input}
                        />
                    </div>
                    <div style={styles.inputContainer}>
                        <input
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            style={styles.input}
                        />
                    </div>
                    <div>
                        <button type="submit" style={styles.loginButton}>
                            Login
                        </button>
                    </div>
                </form>
            </Shaking>
        </div>
    );
};

export default LoginForm;
