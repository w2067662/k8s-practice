import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <ProductBox width={width} height={height} category={category} mageUrl={mageUrl} title={title} content={content}/>
//-------------------------------------------------------------------
const ProductBox = ({ width = 300, height = 300, category, imageUrl, title, content }) => {
    return (
        <motion.div
            style={{
                position: 'relative',
                width: `${width}px`,
                height: `${height}px`,
                overflow: 'hidden',
                margin: '10px',
            }}
        >
            <div
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <p style={{ color: '#fff', fontWeight: 'bold', fontSize: '1.5rem' }}>{title}</p>
                <p
                    style={{
                        color: '#fff',
                        fontSize: '1rem',
                        margin: 0,
                    }}
                >
                    {content}
                </p>
            </div>
            <img
                src={imageUrl}
                alt={category}
                style={{
                    width: '100%',
                    height: '100%',
                    objectFit: 'cover',
                }}
            />
            <motion.div
                initial={{ opacity: 0 }}
                whileHover={{ opacity: 1 }}
                transition={{ duration: 0.5 }}
                style={{
                    position: 'absolute',
                    top: '5px',
                    left: '5px',
                    right: '5px',
                    bottom: '5px',
                    backgroundColor: '#ff000088',
                    opacity: 0,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <motion.button
                    whileHover={{ color: 'red', backgroundColor: 'white' }}
                    style={{
                        padding: '10px',
                        backgroundColor: 'transparent',
                        color: 'white',
                        cursor: 'pointer',
                        border: '2px solid white',
                    }}
                >
                    Discover More
                </motion.button>
            </motion.div>
        </motion.div>
    );
};

export default ProductBox;