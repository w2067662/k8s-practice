import React, { useState, useEffect } from 'react';

//-------------------------------------------------------------------
// Usage: 
//      <ProgressBar />
//-------------------------------------------------------------------
const ProgressBar = () => {
    const [scrollPercentage, setScrollPercentage] = useState(0);

    useEffect(() => {
        const handleScroll = () => {
            const windowHeight = window.innerHeight;
            const scrollHeight = document.documentElement.scrollHeight - windowHeight;
            const scrollTop = window.scrollY;
            const percentage = (scrollTop / scrollHeight) * 100;
            setScrollPercentage(percentage);
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    const progressBarStyle = {
        position: 'fixed',
        top: 0,
        left: '50%',
        transform: 'translateX(-50%)', // Center the progress bar horizontally
        width: `${scrollPercentage}%`,
        height: '4px',
        backgroundColor: '#f0803c', // Red color
        zIndex: 1000,
        transition: 'width 0.3s ease', // Add transition for a smoother effect
    };

    return (
        <div style={progressBarStyle}></div>
    );
};

export default ProgressBar;
