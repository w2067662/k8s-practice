//-------------------------------------------------------------------
// Usage: 
//      <DigitSVG digit={1~9}, fontSize={fontSize} color={color} />
//-------------------------------------------------------------------
const DigitSVG = ({ digit, fontSize = 40, color = '#f0ebd6' }) => {
    return (
        <svg width={fontSize} height={fontSize} xmlns="http://www.w3.org/2000/svg">
            {/* Example: Display the digit as text */}
            <text x="50%" y="50%" fontSize={fontSize} textAnchor="middle" dy="0.3em" fill={color}>
                {digit}
            </text>
            {/* Add other SVG elements or shapes as needed */}
        </svg>
    );
};

export default DigitSVG;