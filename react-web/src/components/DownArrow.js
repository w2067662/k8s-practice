//-------------------------------------------------------------------
// Usage: 
//      <DownArrow />
//-------------------------------------------------------------------
const DownArrow = () => {
    const arrowStyle = {
        width: 0,
        height: 0,
        borderTop: '20px solid #f0ebd6',
        borderLeft: '10px solid transparent',
        borderRight: '10px solid transparent'
    };

    return (
        <div style={arrowStyle} />
    );
};

export default DownArrow;
