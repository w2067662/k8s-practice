import React, { useState } from 'react';
import './PingButton.css';

//-------------------------------------------------------------------
// Usage: 
//      <PingButton onPingResponse={onPingResponse}/>
//-------------------------------------------------------------------
const PingButton = ({ onPingResponse }) => {
    const [buttonText, setButtonText] = useState('Ping'); // Initialize the button text

    const getRandomService = () => {
        const services = ['go', 'rust'];
        return services[Math.floor(Math.random() * services.length)];
    };

    const handlePing = async () => {
        if (buttonText === 'Ping') {
            try {
                const service = getRandomService();
                const request = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    credentials: 'include', // Include credentials for cross-origin requests
                };

                const response = await fetch(`http://${window.location.host}/${service}/ping`, request);

                if (response.status === 204) {
                    // Request was successful, and there's no content
                    console.log('Ping successful, no content');
                    onPingResponse('has no content from'); // Pass response to the parent component
                } else if (response.ok) {
                    // Request was successful, you can handle the response here
                    const data = await response.text(); // Parse the response as text
                    console.log('successful at', data);
                    onPingResponse(data); // Pass response to the parent component
                } else {
                    // Request failed, handle the error
                    console.error('Ping failed');
                    onPingResponse('failed at'); // Pass response to the parent component
                }
            } catch (error) {
                // An error occurred, handle it here
                console.error('An error occurred:', error);
                onPingResponse('error occurred to'); // Pass response to the parent component
            }

            setButtonText('Reset'); // Change button text when the request is sent
        } else if (buttonText === 'Reset') {
            setButtonText('Ping'); // Change button text back to "Ping"
            onPingResponse(''); // Clear the response in the parent component
        }
    };

    return (
        <div className="container">
            <div className="button-container">
                <button type="button" className="btn cube cube-hover" onClick={handlePing}>
                    <div className="bg-top">
                        <div className="bg-inner"></div>
                    </div>
                    <div className="bg-right">
                        <div className="bg-inner"></div>
                    </div>
                    <div className="bg">
                        <div className="bg-inner"></div>
                    </div>
                    <div className="text">{buttonText}</div>
                </button>
            </div>
        </div>
    );
};

export default PingButton;
