import React from 'react';

//-------------------------------------------------------------------
// Usage: 
//      <Footer />
//-------------------------------------------------------------------
const Footer = () => {
    return (
        <footer style={footerStyle}>
            <p>@react-web</p>
        </footer>
    );
};

const footerStyle = {
    fontFamily: 'Rubik, sans-serif',
    color: '#f0ebd6',
    fontSize: '14px',
    textAlign: 'center',
    padding: '10px',
    position: 'fixed',
    width: '100%',
    bottom: '0',
    zIndex: '1000',
};

export default Footer;
