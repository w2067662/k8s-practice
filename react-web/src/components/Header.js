import React from 'react';
import { Link } from 'react-router-dom';
import paintPNG from '../png/paint.png';
import Menu from './Menu';

//-------------------------------------------------------------------
// Usage: 
//      <Header />
//-------------------------------------------------------------------
const Header = () => {
    return (
        <header style={headerStyle}>
            <Link to="/">
                <img
                    src={paintPNG}
                    alt="Paint"
                    style={{ width: '50px', marginRight: '10px', cursor: 'pointer' }}
                />
            </Link>
            <p>Frontend</p> {/* Add your header content here */}
            <Menu />
        </header>
    );
};

const headerStyle = {
    fontFamily: 'Rubik, sans-serif',
    color: '#f0ebd6',
    textAlign: 'center',
    padding: '10px',
    position: 'fixed',
    width: '100%',
    top: '0',
    zIndex: '1000',
    display: 'flex', // Set display to flex
    alignItems: 'center', // Center items vertically
};

export default Header;
