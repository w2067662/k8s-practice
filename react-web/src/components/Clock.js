import React, { useState, useEffect } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import DigitSVG from './DigitSVG';

//-------------------------------------------------------------------
// Usage: 
//      <Clock fontSize={fontSize} />
//-------------------------------------------------------------------
const Clock = ({ fontSize = 25 }) => {
    const [time, setTime] = useState(new Date());

    useEffect(() => {
        const intervalId = setInterval(() => {
            setTime(new Date());
        }, 1000);

        return () => {
            clearInterval(intervalId);
        };
    }, []); // The empty dependency array ensures that the effect runs only once when the component mounts

    const formattedTime = () => {
        const hours = time.getHours().toString().padStart(2, '0');
        const minutes = time.getMinutes().toString().padStart(2, '0');
        const seconds = time.getSeconds().toString().padStart(2, '0');
        return `${hours}:${minutes}:${seconds}`;
    };

    const charWidth = fontSize * 0.6;
    const totalWidth = formattedTime().length * charWidth;
    const containerStyle = {
        margin: '10px',
        position: 'relative',
        width: `${totalWidth}px`,
        height: `${fontSize * 1}px`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center', // Center the content vertically
    };

    const charStyles = {
        position: 'absolute',
        margin: '0px', // Adjust the margin as a percentage of fontSize
        width: `${charWidth}px`, // Adjust the width as a percentage of fontSize
        height: `${fontSize * 0.1}px`, // Adjust the height as a percentage of fontSize
        fontFamily: 'Rubik, sans-serif',
        fontSize: `${fontSize}px` || 'inherit',
        display: 'flex',
        alignItems: 'center', // Center the content vertically
    };

    const fadeInMotion = {
        hidden: { opacity: 0 },
        visible: { opacity: 1, transition: { duration: 0.1, delay: 0.3 } },
    };

    return (
        <div style={containerStyle}>
            {formattedTime().split('').map((char, index) => (
                <AnimatePresence key={index}>
                    <motion.p
                        style={{ ...charStyles, left: `${index * charWidth}px` }}
                        key={char}
                        variants={fadeInMotion}
                        initial="hidden"
                        animate="visible"
                        exit="hidden"
                    >
                        <DigitSVG digit={char} fontSize={fontSize} />
                    </motion.p>
                </AnimatePresence>
            ))}
        </div>
    );
};

export default Clock;
