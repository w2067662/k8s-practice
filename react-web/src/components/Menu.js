import React, { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Link } from 'react-router-dom';
import './Menu.css';
import Hover from '../motions/Hover';

//-------------------------------------------------------------------
// Usage: 
//      <Menu />
//-------------------------------------------------------------------
const Menu = () => {
    const [isMenuOpen, setMenuOpen] = useState(false);

    const handleMenuToggle = () => {
        setMenuOpen(!isMenuOpen);
    };

    const closeMenu = () => {
        setMenuOpen(false);
    };

    const menuItems = [
        { label: 'Ping', path: '/ping' },
        { label: 'Go App', path: '/goapp' },
        { label: 'Rust App', path: '/rustapp' },
    ];

    return (
        <div className="menu-container">
            <motion.div
                className={`menu-button ${isMenuOpen ? 'open' : ''}`}
                onClick={handleMenuToggle}
                initial={false}
                animate={{ backgroundColor: isMenuOpen ? '#f0ebd6' : 'rgba(240, 235, 214, 0)' }}
                onMouseEnter={() => setMenuOpen(true)}
                onMouseLeave={() => setMenuOpen(false)}
            >
                {/* You can customize the icon inside the circle here */}
            </motion.div>

            <AnimatePresence>
                {isMenuOpen && (
                    <MenuList menuItems={menuItems} setMenuOpen={setMenuOpen} closeMenu={closeMenu} />
                )}
            </AnimatePresence>
        </div>
    );
};

const MenuList = ({ menuItems, setMenuOpen, closeMenu }) => (
    <motion.div
        className="menu-list"
        initial={{ opacity: 0, y: -20 }}
        animate={{ opacity: 1, y: 0 }}
        exit={{ opacity: 0, y: -20 }}
        onMouseEnter={() => setMenuOpen(true)}
        onMouseLeave={closeMenu}
    >
        {menuItems.map((item, index) => (
            <MenuItem key={index} item={item} index={index} />
        ))}
    </motion.div>
);

const MenuItem = ({ item, index }) => (
    <motion.div
        key={index}
        className="menu-list-item-wrapper"
        variants={{
            hidden: { opacity: 0, y: -20 },
            visible: { opacity: 1, y: 0 },
        }}
        initial="hidden"
        animate="visible"
        transition={{ delay: index * 0.1 }}
    >
        <Hover>
            <Link to={item.path}>
                <AnimatedButton label={item.label} />
            </Link>
        </Hover>
    </motion.div>
);

const AnimatedButton = ({ label }) => (
    <motion.button
        className="menu-list-item"
        whileTap={{ scale: 0.9 }}
        whileHover={{ scale: 1.1 }}
    >
        {label}
    </motion.button>
);

export default Menu;
