import React, { useState } from 'react';
import Shaking from '../motions/Shaking';
import { useNavigate } from 'react-router-dom'; // Import useNavigate
import { ServerEndPoint } from '../env';

//-------------------------------------------------------------------
// Usage: 
//      <SignUpForm />
//-------------------------------------------------------------------
function SignUpForm({ service = 'go', redirectRoute = '/' }) {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [shake, setShake] = useState(false);
    const navigate = useNavigate(); // Initialize useNavigate

    // Set default service to 'go' if not 'go' or 'rust'
    const validServices = ['go', 'rust'];
    const defaultService = validServices.includes(service) ? service : 'go';

    const handleSignup = async (e) => {
        e.preventDefault();

        // Create a request object
        const request = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name, email, password }),
            credentials: 'include', // Include credentials for cross-origin requests
        };

        try {
            // Send the POST request to your signup endpoint
            const response = await fetch(`http://${window.location.host}/${defaultService}/signup`, request);

            if (response.ok) {
                // Signup was successful, you can handle the response here
                const data = await response.json();
                console.log('Signup successful', data);
                // Redirect to the specified route upon successful signup
                navigate(redirectRoute);
            } else {
                // Signup failed, handle the error
                console.error('Signup failed');
                setShake(true);

                // Reset shake after shaking animation completes
                setTimeout(() => {
                    setShake(false);
                }, 500); // Duration of the shaking animation
            }
        } catch (error) {
            // An error occurred, handle it here
            console.error('An error occurred:', error);
            setShake(true);

            // Reset shake after shaking animation completes
            setTimeout(() => {
                setShake(false);
            }, 500); // Duration of the shaking animation
        }

        // Reset the form fields
        setName('');
        setEmail('');
        setPassword('');
    };

    const styles = {
        container: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '50%',
        },
        form: {
            backgroundColor: 'transparent',
            borderRadius: '15px',
            padding: '20px',
            //boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
            //border: '4px solid #f0ebd6',
        },
        inputContainer: {
            marginBottom: '15px',
        },
        input: {
            width: '88%',
            padding: '10px',
            borderRadius: '5px',
            border: '2px solid #f0ebd6',
            backgroundColor: 'transparent',
            color: '#f0ebd6',
            caretColor: '#f0ebd6',
            placeholder: {
                color: '#f0ebd6', // Set placeholder color to #f0ebd6
            },
        },
        signupButton: {
            width: '100%',
            padding: '10px',
            backgroundColor: '#f0ebd6',
            color: '#282c34',
            border: 'none',
            borderRadius: '5px',
            cursor: 'pointer',
        },
    };

    return (
        <div style={styles.container}>
            <Shaking shake={shake} onAnimationComplete={() => setShake(false)}>
                <form onSubmit={handleSignup} style={styles.form}>
                    <div style={styles.inputContainer}>
                        <input
                            type="text"
                            placeholder="name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            required
                            style={styles.input}
                        />
                    </div>
                    <div style={styles.inputContainer}>
                        <input
                            type="email"
                            placeholder="Email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                            style={styles.input}
                        />
                    </div>
                    <div style={styles.inputContainer}>
                        <input
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            style={styles.input}
                        />
                    </div>
                    <div>
                        <button type="submit" style={styles.signupButton}>
                            Sign Up
                        </button>
                    </div>
                </form>
            </Shaking>
        </div>
    );
}

export default SignUpForm;
