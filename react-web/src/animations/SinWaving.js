import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <SinWaving width={width} amplitudeProp={amplitude} frequencyProp={frequency} speed={speed} direction={ || -1} />
//-------------------------------------------------------------------
const SinWaving = ({ width = 800, amplitudeProp = 30, frequencyProp = 4, speed = 10, direction = 1 }) => {
    const [time, setTime] = useState(0);
    const [path, setPath] = useState('');

    const amplitude = amplitudeProp; // Reduce amplitude for more peaks and troughs
    const frequency = 2 * Math.PI / frequencyProp; // Adjust the frequency as needed

    useEffect(() => {
        const generateWave = () => {
            const points = [];
            const cycleWidth = width; // Use the provided width
            for (let x = 0; x <= cycleWidth; x++) {
                const y = amplitude * Math.sin(direction * frequency * x - speed * time);
                points.push(`${x},${amplitude + y}`);
            }
            return points.join(' ');
        };

        const updatePath = () => {
            const newPath = generateWave();
            setPath(newPath);
        };

        const animationFrame = requestAnimationFrame(() => {
            setTime((prevTime) => (prevTime + 0.02));
            updatePath();
        });

        return () => cancelAnimationFrame(animationFrame);
    }, [time, amplitude, frequency, speed, width, direction]);

    return (
        <svg width={width}>
            <motion.path
                d={`M${path}`} // Enclosed cycle path
                initial={{ pathLength: 0 }}
                animate={{ pathLength: 1 }}
                style={{
                    stroke: '#f0ebd6',
                    strokeWidth: 2,
                    fill: 'transparent',
                    display: 'inline-block',
                }}
            />
        </svg>
    );
};

export default SinWaving;
