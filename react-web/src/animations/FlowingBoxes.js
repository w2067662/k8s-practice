import React, { useEffect, useState } from 'react';

//-------------------------------------------------------------------
// Usage: 
//      <FlowingBoxes numBoxes={N} boxSize={size} duration={duration} margin={margin} borderRadius={borderRadius} />
//-------------------------------------------------------------------
const FlowingBoxes = ({ numBoxes = 5, boxSize = 30, duration = 50, margin = 50, borderRadius = 12 }) => {
    const [currentBox, setCurrentBox] = useState(0);

    useEffect(() => {
        const intervalId = setInterval(() => {
            setCurrentBox((prevBox) => (prevBox + 1) % numBoxes);
        }, duration * 10);

        return () => clearInterval(intervalId);
    }, [currentBox, numBoxes, duration]);

    const boxes = Array.from({ length: numBoxes }, (_, index) => {
        const isFilled = index === currentBox;
        const boxStyle = {
            width: boxSize,
            height: boxSize,
            margin: margin,
            borderRadius: borderRadius,
            border: `2px solid ${isFilled ? 'transparent' : '#f0ebd6'}`,
            backgroundColor: isFilled ? '#f0ebd6' : 'transparent',
        };

        return <div key={index} style={boxStyle}></div>;
    });

    return <div style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }}>
        {boxes}
    </div>;
};

export default FlowingBoxes;
