import React, { useEffect, useState } from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <Waving width={width} amplitudeProp={amplitude} frequencyProp={frequency} />
//-------------------------------------------------------------------
const Waving = ({ width = 800, amplitudeProp = 30, frequencyProp = 4 }) => {
    const [time, setTime] = useState(0);

    const amplitude = amplitudeProp; // Reduce amplitude for more peaks and troughs
    const frequency = 2 * Math.PI / frequencyProp; // Adjust the frequency as needed

    // Function to generate a sine wave for animation
    const generateSineWave = (time) => {
        return amplitude * Math.sin(frequency * time);
    };

    const generateD = (width) => {
        let d = '';
        const curvature = `50`;
        for (let x = 0; x <= width; x += 20) {
            let sin = (x / 20) % 2 === 0 ? 50 + generateSineWave(time) : 50 - generateSineWave(time);
            d += `${x} ${curvature} Q ${x + 10} ${sin} `;
        }

        return `M${d}, ${width} 50`;
    };

    useEffect(() => {
        // Update the time every frame for continuous animation
        const animationFrame = requestAnimationFrame(() => {
            setTime((prevTime) => prevTime + 0.02); // Adjust the speed of the wave by changing the increment
        });

        return () => cancelAnimationFrame(animationFrame); // Cleanup on component unmount
    }, [time]);

    return (
        <svg width={width}>
            <motion.path
                d={generateD(width)}
                initial={{ pathLength: 0 }}
                animate={{ pathLength: 1 }}
                style={{
                    stroke: '#f0ebd6',
                    strokeWidth: 2,
                    fill: 'transparent',
                    display: 'inline-block',
                }}
            />
        </svg>
    );
};

export default Waving;
