import React, { useState } from 'react';
import { motion } from 'framer-motion';

//-------------------------------------------------------------------
// Usage: 
//      <RespondingBoxes m={m} n={n} size={size} margin={margin} mode={'Waving' || 'Filled'} />
//-------------------------------------------------------------------
const RespondingBoxes = ({ m = 5, n = 5, size = 50, margin = 5, mode = 'Waving' }) => {
    const [matrix, setMatrix] = useState(Array.from({ length: m }, () => Array(n).fill(false)));

    const boxVariants = {
        filled: {
            rotate: [0, -20, 20, -20, 20, 0],
            backgroundColor: 'red', // Adjust the color as needed
            transition: { duration: 0.5 }, // Adjust the duration for the waving animation
        },
        hollow: {
            backgroundColor: 'transparent',
            transition: { duration: 0.2 }, // Adjust the duration for the exit transition
        },
    };

    const handleFilledClick = (row, col) => {
        const newMatrix = matrix.map((rowArr, rowIndex) =>
            rowArr.map((box, colIndex) => (rowIndex === row && colIndex === col ? !box : box))
        );

        setMatrix(newMatrix);
    };

    const handleArrowClick = (row, col) => {
        const steps = [1, 0, -1, 0, 1];
        let visited = Array.from({ length: m }, () => Array(n).fill(false));
        let list = [[{ row, col }]];

        for (let t = 0; t < list.length; t++) {
            const currentList = list[t];
            let nextList = [];

            for (let i = 0; i < currentList.length; i++) {
                const curr = currentList[i];
                visited[curr.row][curr.col] = true;

                for (let s = 0; s < steps.length - 1; s += 1) {
                    const next = { row: curr.row + steps[s], col: curr.col + steps[s + 1] };

                    if (0 <= next.row && 0 <= next.col && next.row < m && next.col < n) {
                        if (!visited[next.row][next.col]) {
                            visited[next.row][next.col] = true;
                            nextList.push(next);
                        }
                    }
                }
            }

            if (nextList.length === 0) break; // No valid steps, exit the loop

            list.push(nextList);
        }

        // Use an asynchronous function to handle the timeouts and updates
        const updateMatrixWithDelay = async () => {
            for (let i = 0; i < list.length; i++) {
                await new Promise(resolve => setTimeout(resolve, i * 10));

                let newMatrix = matrix.map(rowArr => [...rowArr]);

                for (let j = 0; j < list[i].length; j++) {
                    const { row, col } = list[i][j];
                    newMatrix[row][col] = true; // Set to true to fill the box
                }

                setMatrix(newMatrix);
            }

            setTimeout(() => {
                // After the animation, set all boxes to false
                const finalMatrix = matrix.map(rowArr => rowArr.map(() => false));
                setMatrix(finalMatrix);
            }, (list.length + 1) * 10);
        };

        updateMatrixWithDelay();
    };

    const handleWavingClick = (row, col) => {
        const steps = [1, 0, -1, 0, 1, 1, -1, -1, 1];
        let visited = Array.from({ length: m }, () => Array(n).fill(false));
        let list = [[{ row, col }]];

        for (let t = 0; t < list.length; t++) {
            const currentList = list[t];
            let nextList = [];

            for (let i = 0; i < currentList.length; i++) {
                const curr = currentList[i];
                visited[curr.row][curr.col] = true;

                for (let s = 0; s < steps.length - 1; s += 1) {
                    const next = { row: curr.row + steps[s], col: curr.col + steps[s + 1] };

                    if (0 <= next.row && 0 <= next.col && next.row < m && next.col < n) {
                        if (!visited[next.row][next.col]) {
                            visited[next.row][next.col] = true;
                            nextList.push(next);
                        }
                    }
                }
            }

            if (nextList.length === 0) break; // No valid steps, exit the loop

            list.push(nextList);
        }

        // Use an asynchronous function to handle the timeouts and updates
        const updateMatrixWithDelay = async () => {
            for (let i = 0; i < list.length; i++) {
                await new Promise(resolve => setTimeout(resolve, i * 10));

                let newMatrix = matrix.map(rowArr => [...rowArr]);

                for (let j = 0; j < list[i].length; j++) {
                    const { row, col } = list[i][j];
                    newMatrix[row][col] = true; // Set to true to fill the box
                }

                setMatrix(newMatrix);
            }

            setTimeout(() => {
                // After the animation, set all boxes to false
                const finalMatrix = matrix.map(rowArr => rowArr.map(() => false));
                setMatrix(finalMatrix);
            }, (list.length + 1) * 10);
        };

        updateMatrixWithDelay();
    };

    const handleBoxClick = (row, col) => {
        switch (mode) {
            case 'Arrow':
                handleArrowClick(row, col);
                break;
            case 'Waving':
                handleWavingClick(row, col);
                break;
            case 'Filled':
                handleFilledClick(row, col);
                break;
            default:
            // No Effect on Clicking
        }
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            {[...Array(m)].map((_, rowIndex) => (
                <div key={rowIndex} style={{ display: 'flex' }}>
                    {[...Array(n)].map((_, colIndex) => (
                        <motion.div
                            key={colIndex}
                            style={{
                                width: `${size}px`, // Adjust the width as needed
                                height: `${size}px`, // Adjust the height as needed
                                margin: `${margin}px`,
                                border: '2px solid #f0ebd6', // Adjust the border style as needed
                                borderRadius: '15px',
                                cursor: 'pointer',
                            }}
                            initial={matrix[rowIndex][colIndex] ? 'filled' : 'hollow'}
                            animate={matrix[rowIndex][colIndex] ? 'filled' : 'hollow'}
                            whileHover="filled"
                            onClick={() => handleBoxClick(rowIndex, colIndex)}
                            variants={boxVariants}
                        />
                    ))}
                </div>
            ))}
        </div>
    );
};

export default RespondingBoxes;
