# Buile the application
npm run build

# Start the application
npm start

# Build docker image
docker build -t benenjosoft/react-web:v1.0 .

# Run docker image
docker run -itd benenjosoft/react-web:v1.0

# Run docker image on Port 80
docker run -p 80:80 benenjosoft/react-web:v1.0