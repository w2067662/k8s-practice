# 🌌 React Web

This is my React Web showcase, where I craft a Responsive Web Design (RWD) style website enriched with captivating motion effects.

This project represents my exploration and experimentation with React, demonstrating its capabilities in building responsive and visually dynamic web applications. I've created this showcase not only to illustrate the power of React but also as a personal endeavor to enhance my skills in Responsive Web Design.

I classify **`Reactives`**, **`Animations`**, **`Components`**, **`Motions`**, and **`Pages`** for more straightforward code reading in this project. The following shows a small part of the elements I constructed for this project, along with previewing gifs to demonstrate their effects:



# ☀️ Reactives
**`Reactives`** are interactive components designed to respond to user actions, including clicking, hovering, and more. These dynamic elements can be customized through parameters, allowing you to define the styles and details of the components based on your specific needs.


## ✨ RespondingBoxes
```js
<RespondingBoxes m={m} n={n} size={size} margin={margin} mode={'Arrow' || 'Waving' || 'Filled'} />
```
![responding boxes1](gif/responding_boxes1.gif)
![responding boxes2](gif/responding_boxes2.gif)


# 🌜 Animations
**`Animations`** are components (lines, circles, squares, etc.) that have their own motions and are able to change the attributes of the effects (width, frequency, amplitude, duration, etc.) using parameters.


## ✨ Waving
```js
<Waving width={width} amplitudeProp={amplitude} frequencyProp={frequency} />
```
![waving](gif/waving.gif)


## ✨ SinWaving
```js
<SinWaving width={width} amplitudeProp={amplitude} frequencyProp={frequency} speed={speed} direction={1 || -1} />
```
![sin waving](gif/sin_waving.gif)


## ✨ FlowingBoxes
```js
<FlowingBoxes numBoxes={N} boxSize={size} duration={duration} margin={margin} borderRadius={borderRadius} />
```
![flowing boxes](gif/flowing_boxes.gif)



# 🪐 Motions
Different from Animation, **`Motions`** are callback functions that can be applied to children. It is possible to enclose the components within motions to apply the effects.

## ✨ Typing
```js
<Typing text={text} />
```
![shaking](gif/typing.gif)


## ✨ Shaking
```js
<Shaking shake={shake} onAnimationComplete={() => func()}> {children} </Shaking>
```
![shaking](gif/shaking.gif)


## ✨ Shinning
```js
<Shining> {children} </Shining>
```
![shinning](gif/shinning.gif)


## ✨ JumpOnHover
```js
<JumpOnHover> {children} </JumpOnHover>
```
![jump on hover](gif/jump_on_hover.gif)


## ✨ RotateOnHover
```js
<RotateOnHover> {children} </RotateOnHover>
```
![rotate on hover](gif/rotate_on_hover.gif)


## ✨ DisplayLoop
```js
<DisplayLoop duration={duration}> {children} </DisplayLoop>
```
![display loop](gif/display_loop.gif)



# 🌠 Components
**`Components`** are UI building blocks that encapsulate specific functionality and visual elements. In the context of the React Web showcase, components play a crucial role in creating a modular and reusable structure for the application.


## ✨ Menu
```js
<Menu />
```
![menu](gif/menu.gif)