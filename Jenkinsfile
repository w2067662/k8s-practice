// This Jenkinsfile build the pipeline for CI/CD of k8s-practice project (Minikube)

// - CI: Clone the repository -> Build Docker images -> Push to Docker-hub
// - CD: Pull the Docker images from Docker-hub -> Deploy the Kubernetes Cluster

// Prerequisite:
// - Set up Jenkins (on WSL)
// - Set up Minikube
// - Add Docker-hub credential with credentialID = "docker-hub" (for Pushing)

pipeline {
    agent any

    environment {
        // Git
        BRANCH = "main"
        REPO_URL = "https://gitlab.com/w2067662/k8s-practice.git"
        // App
        REACT_WEB = "react-web"
        GO_APP = "go-app"
        RUST_APP = "rust-app"
        // Image
        IMAGE_VERSION = "latest"
        // Docker-Hub
        DOCKER_HUB_NAME = "benenjosoft"
        // K8S
        KUBE_CLUSTER_NAME = "minikube"
    }
    
    stages {
        stage("Clone the Repository") {
            steps {
                // Clone the Git repository
                git branch: "$BRANCH", url: "$REPO_URL"
                sh "ls"
            }
        }

        stage("Build and Push Docker Images") {
            steps {
                script {
                    // IF Docker permission denied:
                    //      Run "sudo usermod -a -G docker jenkins" on CMD (for Docker permission)
                    //      Run "service jenkins restart" to refresh jenkins config

                    // Build Docker images
                    sh "docker build -t $DOCKER_HUB_NAME/$REACT_WEB:$IMAGE_VERSION ./$REACT_WEB"     
	                echo "Build Image $REACT_WEB Completed"
	                sh "docker build -t $DOCKER_HUB_NAME/$GO_APP:$IMAGE_VERSION ./$GO_APP"     
	                echo "Build Image $GO_APP Completed"
	                sh "docker build -t $DOCKER_HUB_NAME/$RUST_APP:$IMAGE_VERSION ./$RUST_APP"     
	                echo "Build Image $RUST_APP Completed"
	                
                    // Login to Docker Hub and push images
                    withCredentials([string(credentialsId: "docker-hub", variable: "dockerHubPwd")]) {
                        sh "docker login -u $DOCKER_HUB_NAME -p ${dockerHubPwd}"
                        
                        sh "docker push $DOCKER_HUB_NAME/$REACT_WEB:$IMAGE_VERSION"           
                        echo "Push Image $REACT_WEB Completed"
                        sh "docker push $DOCKER_HUB_NAME/$GO_APP:$IMAGE_VERSION"           
                        echo "Push Image $GO_APP Completed"
                        sh "docker push $DOCKER_HUB_NAME/$RUST_APP:$IMAGE_VERSION"           
                        echo "Push Image $RUST_APP Completed"
                    }

                }
            }
        }

        stage("Deploy to Kubernetes") {
            steps {
                script {
                    // Apply Kubernetes deployment to Minikube
                    sh "kubectl apply -f deployment.yaml"
                }
            }
        }
    }

    post {
        success {
            echo "Deployment successful!"
            // Notify or trigger additional steps on success
        }
        failure {
            echo "Deployment failed!"
            // Notify or handle failure scenarios
        }
        always {  
	        sh "docker logout"     
        }
    }
}
