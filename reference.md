### React Framer motion Examples:
- [Framer Motion Examples](https://www.framer.com/motion/examples/)

### External services to K8s:
- [External Services to Kubernetes](https://ithelp.ithome.com.tw/articles/10194344)

### Ingress Controller:
- [Ingress Controller](https://ithelp.ithome.com.tw/articles/10196261)

### HTTP Proxy Access K8s API:
- [HTTP Proxy Access to Kubernetes API](https://kubernetes.io/zh-cn/docs/tasks/extend-kubernetes/http-proxy-access-api/)

### Fix CORS problems:
- [What is CORS?](https://www.shubo.io/what-is-cors/#%E4%BB%80%E9%BA%BC%E6%98%AF%E5%90%8C%E6%BA%90)
- [CORS Issues](https://github.com/aszx87410/blog/issues/69)
- [CORS Solutions](https://github.com/aszx87410/blog/issues/70)

### Frontend to Backend on K8s:
- [Connecting Frontend to Backend on Kubernetes](https://kubernetes.io/docs/tasks/access-application-cluster/connecting-frontend-backend/)

### Icons:
- [Icons](https://qiita.com/yosshi_/items/2db0a0e66a16711bfe5f)

### K8s tutorials:
- [Kubernetes Tutorials](https://ithelp.ithome.com.tw/users/20092379/ironman/3220?page=1)

### K8s PV/PVC:
- [Kubernetes PV/PVC Management](https://medium.com/k8s%E7%AD%86%E8%A8%98/kubernetes-k8s-pv-pvc-%E5%84%B2%E5%AD%98%E5%A4%A7%E5%B0%8F%E4%BA%8B%E4%BA%A4%E7%B5%A6pv-pvc%E7%AE%A1%E7%90%86-4d412b8bafb5)

### K8s hostNetwork:
- [Kubernetes Host Network Article 1](https://www.cnblogs.com/zhangmingcheng/p/17640118.html)
- [Kubernetes Host Network Article 2](https://cloud.tencent.com/developer/article/2171254)

### K8s client-node:
- [Kubernetes Client Node](https://www.npmjs.com/package/@kubernetes/client-node)

### Jenkins:
- [Jenkins - Install on WSL](https://medium.com/try-except-finally/install-jenkins-on-wsl-ubuntu-d6cfeec8cd60)
- [Jenkins - Install on Docker](https://ithelp.ithome.com.tw/articles/10200621)
- [Jenkins - Add Credentials](https://www.youtube.com/watch?v=mRRS3gcC800)
- [Jenkins - Docker Permission Denied](https://www.baeldung.com/linux/docker-permission-denied-daemon-socket-error)
- [Jenkins - Integrate with Telegram bot](https://kodekloud.com/community/t/integrate-jenkins-with-telegram-bot/316502)
- [Jenkins - CICD Pipeline To Deploy To Kubernetes](https://www.youtube.com/watch?v=XE_mAhxZpwU)

### Harness
- [CI/CD Tutorial](https://dev.to/pavanbelagatti/cicd-tutorial-for-developers-3l0)
